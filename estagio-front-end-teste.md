**Teste para estágio em desenvolvimento front-end**

##### Teste 1
Escreva um algoritmo que armazene o valor 10 em uma variável A e o valor 20 em uma variável B.  
Em seguida (utilizando apenas atribuições entre variáveis) troque os seus valores fazendo com que o valor que está em A passe para B e vice-versa.  
Ao final, exibir os valores que ficaram armazenados nas variáveis.  

-----
  
##### Teste 2

Escreva um algoritmo que conte de 1 a 20 e coloque ao lado do número se ele é par ou ímpar, e a contagem da quantidade de numeros pares, e ímpares.  
**Ex.:**  
1: ímpar  
2: par  
3: ímpar  
…  
19: ímpar  
20: par  

Contagem:  
XX Pares  
XX Ímpares  

----

**Quando terminar, coloque os testes em um repositório privado no Gitlab, Github ou Bitbucket, e nos envie
por email para e vagas-ti@eotica.com.br**

Será avaliádo estrutura de código, capacidade de resolução de problemas, e nível de lógica dentro da construção de cada algoritimo,  
sinta-se livre pra escolher a linguagem a ser utilizada no teste (preferência por Javacript, mas não é obrigatório)
