**Cargo:** Estágio em Desenvolvimento Front-end  

**O que esperamos de você:**

* Capacidade de aprender coisas novas;
* Espírito de colaboração com a equipe;
* Atenção em tudo o que você está desenvolvendo;
* Conhecimento Básico de JavaScript;
* Conhecimento em HTML e CSS;
* Boa lógica de programação


**Diferenciais:**

* Sugerir novas e melhores soluções em tudo que inicia;
* Conhecimento de SEO e boas práticas de código semântico;
* Experiência com Git;
* Conhecimento em pré-processadores CSS;
* Conhecimento de conexão com APIs;


---
**Bolsa e benefícios**

Carga horária: 6 horas  (horários 8h às 15h, 9h às 16h ou 10h às 17h)  
R$1.200 / mês,    
Vale Refeição,  
Vale Transporte,  
Convênio médico

**Localização**

São Paulo/SP - Bairro Jaguaré


**Como se candidatar**  
[Faça esse pequeno teste](https://gitlab.com/e3optical-public/front-end/blob/master/estagio-front-end-teste.md)