**Cargo:** Desenvolvedor Front-end Pleno/Sênior

**O que esperamos de você:**

Ser a cabeça do desenvolvimento front-end dos produtos da e3group,  
e estar alinhado diretamente com a liderança técnica de back-end e UX;  
Propor melhorias contínuas nos produtos e ter uma visão generalista no  
sentido de agregar conhecimento tanto para o time de desenvolvimento  
quanto para os produtos em si.

* Capacidade de aprender coisas novas;  
* Espírito de colaboração com a equipe;  
* Atenção em tudo o que você está desenvolvendo;  
* Experiência em JavaScript;  
* Experiência em HTML e CSS;  
* Experiência em Git;  
* Boa lógica de programação;  
* Código limpo e boas práticas;  
* Reutilização e aproveitamento de código;  
  

**Diferenciais:**

* Experiência com pré-processadores (Stylus e SASS);
* Experiência com Vue/Vuex/Nuxt;
* Experiência em Node;
* Testes;
* Criação, edição e integrações com APIs RESTful;
* Conhecimento de SEO/GTM;

---
**Salário e benefícios**

Carga horária: 44 horas semanais   
R$XXXX / mês,    
Vale Refeição,  
Vale Transporte,  
Convênio médico

**Localização**

São Paulo/SP - Bairro Jaguaré